package com.laituanmanh.test;

import com.laituanmanh.AAOClient.crawler.Score;
import com.laituanmanh.AAOClient.model.ScoreBoard;
import com.laituanmanh.AAOClient.model.SemesterOption;
import junit.framework.TestCase;

import java.io.File;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Manh on 8/29/2015.
 */
public class TKBTest extends TestCase {

    public void testGetSemesterList() throws Exception {
        Score score = new Score();
        List<SemesterOption> semesters = score.getSemesterList();
        System.out.println("DANH SACH HOC KI: "+semesters);
        if(semesters != null) {
            ScoreBoard semesterSubjects = score.getSemesterScore(semesters.get(2), "51202099");
            System.out.println("DANH SACH MON HOC: " + semesterSubjects);

            PrintWriter printWriter = new PrintWriter(new File("Test.txt"),"UTF-8");
            printWriter.println("Tên Môn Học");
            printWriter.print(semesterSubjects);
            printWriter.flush();
        }

    }
}
