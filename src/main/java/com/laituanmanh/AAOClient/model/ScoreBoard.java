package com.laituanmanh.AAOClient.model;

import java.util.List;

/**
 * Created by manh on 11/15/15.
 */
public class ScoreBoard {
    private List<SemesterScore> semesterScores;
    private String studentName;

    public List<SemesterScore> getSemesterScores() {
        return semesterScores;
    }

    public void setSemesterScores(List<SemesterScore> semesterScores) {
        this.semesterScores = semesterScores;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    @Override
    public String toString() {
        return "ScoreBoard{" +
                "semesterScores=" + semesterScores +
                ", studentName='" + studentName + '\'' +
                '}';
    }
}
