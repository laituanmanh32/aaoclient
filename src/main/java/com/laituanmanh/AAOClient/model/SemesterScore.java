package com.laituanmanh.AAOClient.model;

import com.laituanmanh.AAOClient.util.NumberParser;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manh on 30/08/2015.
 */
public class SemesterScore {
    private List<SubjectScore> subjectScores;
    private String semesterName;
    private Integer totalRegisteredCredit;
    private Integer totalPassedCredit;
    private Integer totalCredit;
    private Double semesterGPA;
    private Double totalGPA;

    public SemesterScore() {
        subjectScores = null;
        semesterName = "";
        totalRegisteredCredit = null;
        totalPassedCredit = null;
        totalCredit = null;
        semesterGPA = null;
        totalGPA = null;
    }


    public SemesterScore(Element semesterName, Element scoreTable) {
        this.build(semesterName, scoreTable);
    }

    public List<SubjectScore> getSubjectScores() {
        return subjectScores;
    }

    public void setSubjectScores(List<SubjectScore> subjectScores) {
        this.subjectScores = subjectScores;
    }

    public String getSemesterName() {
        return semesterName;
    }

    public void setSemesterName(String semesterName) {
        this.semesterName = semesterName;
    }

    public Integer getTotalRegisteredCredit() {
        return totalRegisteredCredit;
    }

    public void setTotalRegisteredCredit(Integer totalRegisteredCredit) {
        this.totalRegisteredCredit = totalRegisteredCredit;
    }

    public Integer getTotalPassedCredit() {
        return totalPassedCredit;
    }

    public void setTotalPassedCredit(Integer totalPassedCredit) {
        this.totalPassedCredit = totalPassedCredit;
    }

    public Integer getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(Integer totalCredit) {
        this.totalCredit = totalCredit;
    }

    public Double getSemesterGPA() {
        return semesterGPA;
    }

    public void setSemesterGPA(Double semesterGPA) {
        this.semesterGPA = semesterGPA;
    }

    public Double getTotalGPA() {
        return totalGPA;
    }

    public void setTotalGPA(Double totalGPA) {
        this.totalGPA = totalGPA;
    }

    public void build(Element semesterName, Element scoreTable) {
        this.semesterName = semesterName.text();
        this.subjectScores = new ArrayList<>();

        Elements scoreRows = scoreTable.select("table[align=left] > tbody> tr");
        Elements summaries = scoreTable.select("table table > tbody > tr > td");
        for (int i = 1; i < scoreRows.size() - 1; i++) {
            Element scoreRow = scoreRows.get(i);
            Elements scoreCol = scoreRow.select("td");

            SubjectScore subjectScore = new SubjectScore();
            subjectScore.setSubjectId(scoreCol.get(0).text());
            subjectScore.setSubjectName(scoreCol.get(1).text());
            subjectScore.setGroup(scoreCol.get(2).text());
            subjectScore.setCredit(scoreCol.get(3).text());
            subjectScore.setMidExam(scoreCol.get(4).text());
            subjectScore.setFinalExam(scoreCol.get(5).text());
            subjectScore.setFinalScore(scoreCol.get(6).text());

            subjectScores.add(subjectScore);
//            System.out.println(scoreCol.get(0));
        }

        if (summaries.size() == 5) {
            totalRegisteredCredit = NumberParser.parseInteger(getScore(summaries.get(0).text()));
            totalPassedCredit = NumberParser.parseInteger(getScore(summaries.get(1).text()));
            totalCredit = NumberParser.parseInteger(getScore(summaries.get(2).text()));
            semesterGPA = NumberParser.parseDouble(getScore(summaries.get(3).text()));
            totalGPA = NumberParser.parseDouble(getScore(summaries.get(4).text()));

        }

//        System.out.println(summaries);
//        System.out.println(scoreRows);
//        System.out.println(scoreRows.size());
    }

    private String getScore(String string) {
        return string.split(":")[1];
    }


    @Override
    public String toString() {
        return "SemesterScore{" +
                "subjectScores=" + subjectScores +
                ", semesterName='" + semesterName + '\'' +
                ", totalRegisteredCredit=" + totalRegisteredCredit +
                ", totalPassedCredit=" + totalPassedCredit +
                ", totalCredit=" + totalCredit +
                ", semesterGPA=" + semesterGPA +
                ", totalGPA=" + totalGPA +
                '}';
    }
}
