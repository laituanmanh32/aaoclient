package com.laituanmanh.AAOClient.model;

import java.util.List;

/**
 * Created by manh on 30/08/2015.
 */
public class SemesterTimetable {
    private List<SubjectSchedule> subjectSchedules;

    public SemesterTimetable(List<SubjectSchedule> subjectSchedules) {
        this.subjectSchedules = subjectSchedules;
    }

    public List<SubjectSchedule> getSubjectSchedules() {
        return subjectSchedules;
    }

    public void setSubjectSchedules(List<SubjectSchedule> subjectSchedules) {
        this.subjectSchedules = subjectSchedules;
    }

    @Override
    public String toString() {
        return "SemesterTimetable{" +
                "subjectSchedules=" + subjectSchedules +
                '}';
    }
}
