package com.laituanmanh.AAOClient.model;

/**
 * Created by Manh on 8/30/2015.
 */
public class SemesterOption {
    private String value;
    private String name;

    public SemesterOption() {
    }

    public SemesterOption(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "SemesterOption{" +
                "value='" + value + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
