package com.laituanmanh.AAOClient.model;

/**
 * Created by manh on 30/08/2015.
 */
public class SubjectScore {
    private String subjectId;
    private String subjectName;
    private String group;
    private String credit;
    private String midExam;
    private String finalExam;
    private String finalScore;

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getMidExam() {
        return midExam;
    }

    public void setMidExam(String midExam) {
        this.midExam = midExam;
    }

    public String getFinalExam() {
        return finalExam;
    }

    public void setFinalExam(String finalExam) {
        this.finalExam = finalExam;
    }

    public String getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(String finalScore) {
        this.finalScore = finalScore;
    }

    @Override
    public String toString() {
        return "SubjectScore{" +
                "subjectId='" + subjectId + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", group='" + group + '\'' +
                ", credit='" + credit + '\'' +
                ", midExam='" + midExam + '\'' +
                ", finalExam='" + finalExam + '\'' +
                ", finalScore='" + finalScore + '\'' +
                '}';
    }
}
