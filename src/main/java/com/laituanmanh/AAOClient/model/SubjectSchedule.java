package com.laituanmanh.AAOClient.model;

/**
 * Created by Manh on 8/29/2015.
 */
public class SubjectSchedule {
    private String msmh;
    private String ten;
    private String nhom;
    private String thu;
    private String tiet;
    private String phong;
    private String tuanhoc;

    public SubjectSchedule(String msmh, String ten, String nhom, String thu, String tiet, String phong, String tuanhoc) {
        this.msmh = msmh;
        this.ten = ten;
        this.nhom = nhom;
        this.thu = thu;
        this.tiet = tiet;
        this.phong = phong;
        this.tuanhoc = tuanhoc;
    }

    public String getMsmh() {
        return msmh;
    }

    public void setMsmh(String msmh) {
        this.msmh = msmh;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getNhom() {
        return nhom;
    }

    public void setNhom(String nhom) {
        this.nhom = nhom;
    }

    public String getThu() {
        return thu;
    }

    public void setThu(String thu) {
        this.thu = thu;
    }

    public String getTiet() {
        return tiet;
    }

    public void setTiet(String tiet) {
        this.tiet = tiet;
    }

    public String getPhong() {
        return phong;
    }

    public void setPhong(String phong) {
        this.phong = phong;
    }

    public String getTuanhoc() {
        return tuanhoc;
    }

    public void setTuanhoc(String tuanhoc) {
        this.tuanhoc = tuanhoc;
    }

    @Override
    public String toString() {
        return "SubjectSchedule{" +
                "msmh='" + msmh + '\'' +
                ", ten='" + ten + '\'' +
                ", nhom='" + nhom + '\'' +
                ", thu='" + thu + '\'' +
                ", tiet='" + tiet + '\'' +
                ", phong='" + phong + '\'' +
                ", tuanhoc='" + tuanhoc + '\'' +
                '}';
    }
}
