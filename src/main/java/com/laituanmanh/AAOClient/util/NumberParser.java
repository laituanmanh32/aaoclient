package com.laituanmanh.AAOClient.util;

/**
 * Created by manh on 11/15/15.
 */
public class NumberParser {
    public static Double parseDouble(String number) {
        return Double.parseDouble(number);
    }

    public static Integer parseInteger(String number) {
        return Integer.parseInt(number);
    }
}
