package com.laituanmanh.AAOClient.crawler;

import com.laituanmanh.AAOClient.model.SemesterOption;
import com.laituanmanh.AAOClient.model.SemesterTimetable;
import com.laituanmanh.AAOClient.model.SubjectSchedule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manh on 8/29/2015.
 */
public class Timetable extends AAO {

    public Timetable() throws MalformedURLException {
        url = IfDef.HOST + "xem_tkb";
    }

    public SemesterTimetable getSemesterTimetable(SemesterOption semester, String candidate) {
        try {
            List<SubjectSchedule> subjectSchedules = new ArrayList<SubjectSchedule>();
            Document doc = Jsoup.connect(url)
                    .data("HOC_KY", semester.getValue())
                    .data("mssv", candidate)
                    .userAgent(USER_AGENT)
                    .postDataCharset("utf-8")
                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                    .header("Accept-Language", "en-US,en;q=0.5")
                    .header("Accept-Encoding", "gzip, deflate")
                    .post();
            Elements trs = doc.select("div[align=center] > table").select("tr").select("table").select("tr");
            for (Element tr : trs) {
                Elements tds = tr.select("td");
                String msmh = tds.get(0).text();
                String ten = tds.get(1).text();
                String nhom = tds.get(2).text();
                String thu = tds.get(3).text();
                String tiet = tds.get(4).text();
                String phong = tds.get(5).text();
                String tuan = tds.get(6).text();

                SubjectSchedule subjectSchedule = new SubjectSchedule(msmh, ten, nhom, thu, tiet, phong, tuan);
                subjectSchedules.add(subjectSchedule);
            }


            return new SemesterTimetable(subjectSchedules);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
