package com.laituanmanh.AAOClient.crawler;

import com.laituanmanh.AAOClient.model.SemesterOption;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manh on 8/30/2015.
 */
public abstract class AAO {
    public static final String USER_AGENT = "Mozilla/5.0";
    protected static String url;

    public List<SemesterOption> getSemesterList() {
        try {

            List<SemesterOption> optionList = new ArrayList<SemesterOption>();

            Document doc = Jsoup.parse(new URL(url).openStream(), "UTF-8", url);
            Elements options = doc.select("select > option");

            for (Element opt : options) {
                String value = opt.attr("value");
                String content = opt.text();
                SemesterOption option = new SemesterOption(value,content);
                optionList.add(option);
            }
            return optionList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
