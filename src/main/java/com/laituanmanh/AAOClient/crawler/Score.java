package com.laituanmanh.AAOClient.crawler;

import com.laituanmanh.AAOClient.model.ScoreBoard;
import com.laituanmanh.AAOClient.model.SemesterOption;
import com.laituanmanh.AAOClient.model.SemesterScore;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manh on 30/08/2015.
 */
public class Score extends AAO {
    public Score() {
        url = IfDef.HOST + "xem_bd";
    }

    public ScoreBoard getSemesterScore(SemesterOption semester, String candidate) {
        ScoreBoard scoreBoard = null;
        try {
            Document doc = Jsoup.connect(url)
                    .data("HOC_KY", semester.getValue())
                    .data("mssv", candidate)
                    .userAgent(USER_AGENT)
                    .postDataCharset("utf-8")
                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                    .header("Accept-Language", "en-US,en;q=0.5")
                    .header("Accept-Encoding", "gzip, deflate")
                    .post();
            Elements hk = doc.select("div[align=center] > font > div[align=center]");
            Elements table = doc.select("div[align=center] > table:has(table)");
//            System.out.println(hk);
//            System.out.println(table);
            Element studentName = hk.remove(0);

            List<SemesterScore> semesterScores = new ArrayList<>();
            for (int i = 0; i < hk.size(); i++) {
                SemesterScore semesterScore = new SemesterScore(hk.get(i), table.get(i));
                semesterScores.add(semesterScore);
            }

            scoreBoard = new ScoreBoard();
            scoreBoard.setStudentName(studentName.text());
            scoreBoard.setSemesterScores(semesterScores);

//            System.out.println(studentName.);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return scoreBoard;
    }
}
